package de.kuseru.raphtalia.commands;

import com.mewna.catnip.entity.builder.EmbedBuilder;
import com.mewna.catnip.entity.message.Message;
import lombok.Getter;

public abstract class Command {

    @Getter
    private final String command;
    @Getter
    private final CommandCategory commandCategory;
    @Getter
    private String[] aliases;

    public Command(String command, CommandCategory commandCategory, String... aliases) {
        this.command = command;
        this.commandCategory = commandCategory;
        this.aliases = aliases;
    }

    public abstract void execute(Message message, String[] args);

    protected EmbedBuilder embedBuilder(String title) {
        return new EmbedBuilder()
                .color(0xa36f4b)
                .title(title);
    }
}
