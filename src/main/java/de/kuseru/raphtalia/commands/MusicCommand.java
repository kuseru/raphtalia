package de.kuseru.raphtalia.commands;

import com.mewna.catnip.entity.channel.TextChannel;
import com.mewna.catnip.entity.guild.Guild;
import com.mewna.catnip.entity.message.Message;
import de.kuseru.raphtalia.Raphtalia;
import de.kuseru.raphtalia.music.MusicPlayer;

public abstract class MusicCommand extends Command {

    public MusicCommand(String command, String... aliases) {
        super(command, CommandCategory.MUSIC, aliases);
    }

    @Override
    public void execute(Message message, String[] args) {
        //TODO: handle not connected in voicechannel
        run(message, args, getMusicPlayer(message.guild(), message.channel().asTextChannel()));
    }

    public abstract void run(Message message, String[] args, MusicPlayer musicPlayer);

    private MusicPlayer getMusicPlayer(Guild guild, TextChannel textChannel) {
        return Raphtalia.getInstance().getMusicPlayerHandler().musicPlayer(guild, textChannel);
    }
}
