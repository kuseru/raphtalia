package de.kuseru.raphtalia.commands.music;

import com.mewna.catnip.entity.message.Message;
import de.kuseru.raphtalia.commands.MusicCommand;
import de.kuseru.raphtalia.music.MusicPlayer;

import java.util.Arrays;

public class PlayCommand extends MusicCommand {

    public PlayCommand() {
        super("play", "p");
    }

    @Override
    public void run(Message message, String[] args, MusicPlayer musicPlayer) {
        musicPlayer.connect(message.guild().voiceStates().getById(message.author().id()).channel());
        musicPlayer.loadTrack(args[1]);
    }
}
