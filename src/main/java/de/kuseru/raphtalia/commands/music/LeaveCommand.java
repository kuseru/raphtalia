package de.kuseru.raphtalia.commands.music;

import com.mewna.catnip.entity.message.Message;
import de.kuseru.raphtalia.commands.MusicCommand;
import de.kuseru.raphtalia.music.MusicPlayer;

import java.util.Objects;

public class LeaveCommand extends MusicCommand {

    public LeaveCommand() {
        super("leave", "l");
    }

    @Override
    public void run(Message message, String[] args, MusicPlayer musicPlayer) {
        Objects.requireNonNull(message.guild()).closeVoiceConnection();
    }
}