package de.kuseru.raphtalia.commands.music;

import com.mewna.catnip.entity.message.Message;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import de.kuseru.raphtalia.commands.MusicCommand;
import de.kuseru.raphtalia.music.MusicPlayer;

public class PreviousCommand extends MusicCommand {

    public PreviousCommand() {
        super("previous", "back");
    }

    @Override
    public void run(Message message, String[] args, MusicPlayer musicPlayer) {
        AudioTrack previousTrack = musicPlayer.trackHandler().previousTrack();
        musicPlayer.trackHandler().forcePlay(previousTrack);
    }
}
