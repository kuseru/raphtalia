package de.kuseru.raphtalia.commands.music;

import com.mewna.catnip.entity.message.Message;
import de.kuseru.raphtalia.commands.MusicCommand;
import de.kuseru.raphtalia.music.MusicPlayer;

import java.util.Objects;

public class JoinCommand extends MusicCommand {

    public JoinCommand() {
        super("join", "j");
    }

    @Override
    public void run(Message message, String[] args, MusicPlayer musicPlayer) {
        var voiceChannel = Objects.requireNonNull(message.guild()).voiceStates().getById(message.author().idAsLong()).channel();
        if (voiceChannel != null)
            musicPlayer.connect(voiceChannel);
    }
}
