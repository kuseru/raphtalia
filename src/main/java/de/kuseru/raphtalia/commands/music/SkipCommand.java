package de.kuseru.raphtalia.commands.music;

import com.mewna.catnip.entity.message.Message;
import de.kuseru.raphtalia.commands.MusicCommand;
import de.kuseru.raphtalia.music.MusicPlayer;

public class SkipCommand extends MusicCommand {

    public SkipCommand() {
        super("skip", "sk");
    }

    @Override
    public void run(Message message, String[] args, MusicPlayer musicPlayer) {
//        musicPlayer.getTrackManager().skip();
    }
}
