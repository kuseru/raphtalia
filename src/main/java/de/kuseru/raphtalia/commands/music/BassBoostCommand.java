package de.kuseru.raphtalia.commands.music;

import com.mewna.catnip.entity.builder.EmbedBuilder;
import com.mewna.catnip.entity.message.Message;
import com.sedmelluq.discord.lavaplayer.filter.equalizer.EqualizerFactory;
import de.kuseru.raphtalia.commands.MusicCommand;
import de.kuseru.raphtalia.music.MusicPlayer;

public class BassBoostCommand extends MusicCommand {

    private boolean bassBoost;

    public BassBoostCommand() {
        super("bassboost", "bb", "bass");
        this.bassBoost = false;
    }

    @Override
    public void run(Message message, String[] args, MusicPlayer musicPlayer) {
        var audioPlayer = musicPlayer.trackHandler().audioPlayer();
        bassBoost = !bassBoost;
        if (bassBoost) {
            var equalizerFactory = new EqualizerFactory();
            audioPlayer.setFilterFactory(equalizerFactory);
            equalizerFactory.setGain(0, 2.0F);
            equalizerFactory.setGain(1, 1.4F);
            equalizerFactory.setGain(2, 0.8F);
        } else audioPlayer.setFilterFactory(null);
        var bassMessage = "BassBoost " + (bassBoost ? "enabled" : "disabled");
        message.channel().sendMessage(new EmbedBuilder().color(0xa36f4b).title(bassMessage).build());
    }
}
