package de.kuseru.raphtalia.commands;

import lombok.Getter;

import java.util.LinkedList;

public class CommandHandler {

    @Getter
    private LinkedList<Command> commandList;

    public CommandHandler() {
        this.commandList = new LinkedList<>();
    }

    public Command parseCommand(String input) {
        for (Command command : this.commandList) {
            if (input.equalsIgnoreCase(command.getCommand()))
                return command;
            for (String alias : command.getAliases()) {
                if (input.equalsIgnoreCase(alias))
                    return command;
            }
        }
        return null;
    }

    public void addCommand(Command... commands) {
        for (Command command : commands) {
            if (!this.commandList.contains(command))
                this.commandList.add(command);
        }
    }
}
