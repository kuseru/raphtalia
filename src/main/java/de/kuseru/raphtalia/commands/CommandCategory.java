package de.kuseru.raphtalia.commands;

import lombok.Getter;

public enum CommandCategory {
    DEVELOPER("Developer"),
    MUSIC("Music"),
    GENERAL("General");

    @Getter
    private String category;

    CommandCategory(String category) {
        this.category = category;
    }
}
