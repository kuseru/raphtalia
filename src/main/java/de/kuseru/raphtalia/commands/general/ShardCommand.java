package de.kuseru.raphtalia.commands.general;

import com.mewna.catnip.entity.builder.EmbedBuilder;
import com.mewna.catnip.entity.message.Message;
import de.kuseru.raphtalia.commands.Command;
import de.kuseru.raphtalia.commands.CommandCategory;

public class ShardCommand extends Command {

    public ShardCommand() {
        super("shard", CommandCategory.GENERAL, "shards");
    }

    @Override
    public void execute(Message message, String[] args) {
        var shardManager = message.catnip().shardManager();
        var shardId = (message.guildIdAsLong() >> 22) % shardManager.shardCount();
        var embedBuilder = new EmbedBuilder()
                .color(0xa36f4b)
                .field("shards", String.valueOf(shardManager.shardCount()), true)
                .field("i am shard", String.valueOf(shardId), true);
        message.channel().sendMessage(embedBuilder.build());
    }
}
