package de.kuseru.raphtalia.commands.general;

import com.mewna.catnip.entity.message.Message;
import de.kuseru.raphtalia.commands.Command;
import de.kuseru.raphtalia.commands.CommandCategory;

public class PingCommand extends Command {

    public PingCommand() {
        super("ping", CommandCategory.GENERAL, "pong");
    }

    @Override
    public void execute(Message message, String[] args) {
        final long start = System.nanoTime() / 1_000_000;
        message.channel().sendMessage("pong! | ...ms")
                .thenAccept(pongMessage -> pongMessage.edit(String.format("pong! | %sms", (System.nanoTime() / 1_000_000) - start)));
    }
}
