package de.kuseru.raphtalia.commands.general;

import com.mewna.catnip.entity.message.Message;
import de.kuseru.raphtalia.commands.Command;
import de.kuseru.raphtalia.commands.CommandCategory;
import de.kuseru.raphtalia.util.RuntimeUtil;

public class StatsCommand extends Command {

    public StatsCommand() {
        super("stats", CommandCategory.GENERAL, "statistics");
    }

    @Override
    public void execute(Message message, String[] args) {
        var embed = embedBuilder("Stats")
                .description("Current stats about Raphtalia")
                .field("Cache",
                        "Guilds: " + message.catnip().cache().guilds().size() + "\n" +
                                "Users: " + message.catnip().cache().users().size() + "\n" +
                                "Channels: " + message.catnip().cache().channels().size() + "\n" +
                                "Members: " + message.catnip().cache().members().size(),
                        true)
                .field("Memory",
                        "used: " + RuntimeUtil.usedMemory() + "MB\n" +
                                "free: " + RuntimeUtil.freeMemory() + "MB\n" +
                                "total: " + RuntimeUtil.totalMemory() + "MB\n" +
                                "max: " + RuntimeUtil.maxMemory() + "MB",
                        true)
                .build();
        message.channel().sendMessage(embed);
    }
}
