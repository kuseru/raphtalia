package de.kuseru.raphtalia.config;

import com.electronwill.nightconfig.core.file.FileConfig;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.IOException;

@Log4j2
public class Config {

    private FileConfig fileConfig;

    public Config(String filePath) {
        var file = new File(filePath);
        if (!file.exists()) {
            try {
                //noinspection ResultOfMethodCallIgnored
                file.createNewFile();
            } catch (IOException e) {
                log.error("Failed to create configuration file!", e);
            }
        }
        this.fileConfig = FileConfig.of(file);
        this.fileConfig.load();
        defaultKeys();
    }

    private void defaultKeys() {
        if (!this.fileConfig.contains("settings.prefix"))
            this.fileConfig.set("settings.prefix", "r!");
        if (!this.fileConfig.contains("settings.shards"))
            this.fileConfig.set("settings.shards", 2);
        if (!this.fileConfig.contains("discord.token"))
            this.fileConfig.set("discord.token", "token");
        this.fileConfig.save();
    }

    public FileConfig getFileConfig() {
        return this.fileConfig;
    }
}