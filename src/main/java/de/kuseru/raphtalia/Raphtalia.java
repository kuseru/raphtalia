package de.kuseru.raphtalia;

import com.electronwill.nightconfig.core.file.FileConfig;
import com.github.natanbc.catnipvoice.CatnipVoice;
import com.github.natanbc.catnipvoice.magma.MagmaHandler;
import com.mewna.catnip.Catnip;
import com.mewna.catnip.CatnipOptions;
import com.mewna.catnip.entity.user.Presence;
import com.mewna.catnip.shard.DiscordEvent;
import com.mewna.catnip.shard.manager.DefaultShardManager;
import com.sedmelluq.discord.lavaplayer.jdaudp.NativeAudioSendFactory;
import de.kuseru.raphtalia.commands.CommandHandler;
import de.kuseru.raphtalia.commands.general.PingCommand;
import de.kuseru.raphtalia.commands.general.ShardCommand;
import de.kuseru.raphtalia.commands.general.StatsCommand;
import de.kuseru.raphtalia.commands.music.*;
import de.kuseru.raphtalia.config.Config;
import de.kuseru.raphtalia.music.MusicPlayerHandler;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.IOException;
import java.util.Objects;

@Log4j2
public class Raphtalia {

    @Getter
    private static Raphtalia instance;
    @Getter
    private FileConfig config;
    @Getter
    private Catnip catnip;
    @Getter
    private CommandHandler commandHandler;
    @Getter
    private MusicPlayerHandler musicPlayerHandler;

    private Raphtalia() throws IOException {
        instance = this;
        long start = System.currentTimeMillis();
        Configurator.setRootLevel(Level.INFO);
        Configurator.initialize(ClassLoader.getSystemClassLoader(),
                new ConfigurationSource(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream("log4j2.xml"))));

        log.info("loading configuration file...");
        this.config = new Config("config.toml").getFileConfig();

        log.info("catnipping...");
        CatnipOptions catnipOptions = new CatnipOptions(this.config.get("discord.token")).shardManager(new DefaultShardManager((Integer) this.config.get("settings.shards")));
        catnipOptions.presence(Presence.of(Presence.OnlineStatus.DND, Presence.Activity.of("waking up...", Presence.ActivityType.PLAYING)));
        this.catnip = Catnip.catnip(catnipOptions);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            this.catnip.shutdown();
            this.config.close();
        }));
        this.catnip.loadExtension(new CatnipVoice(new MagmaHandler(new NativeAudioSendFactory())));
        log.info("registering commands...");
        this.commandHandler = new CommandHandler();
        this.commandHandler.addCommand(
                /* General */
                new PingCommand(),
                new ShardCommand(),
                new StatsCommand(),
                /* Music */
                new BassBoostCommand(),
                new ClearQueueCommand(),
                new JoinCommand(),
                new LeaveCommand(),
                new PauseCommand(),
                new PlayCommand(),
                new PreviousCommand(),
                new ResumeCommand(),
                new SkipCommand(),
                new StopCommand()
        );

        log.info("initializing music...");
        this.musicPlayerHandler = new MusicPlayerHandler();

        log.info("registering events...");
        registerEvents();
        this.catnip.connect();
        log.info(String.format("started successfully! (took %sms)", (System.currentTimeMillis() - start)));
    }

    private void registerEvents() {
        this.catnip.on(DiscordEvent.READY, ready -> {
            var user = ready.user();
            log.info(String.format("waked up as %s#%s", user.username(), user.discriminator()));
            this.catnip.game("ready to play~", Presence.ActivityType.PLAYING, null);
        });
        this.catnip.on(DiscordEvent.MESSAGE_CREATE, message -> {
            final String prefix = this.config.get("settings.prefix");
            final String content = message.content().trim();
            if (content.toLowerCase().startsWith(prefix)) {
                final String[] arguments = content.split(" ");
                final String input = arguments[0].replaceFirst(prefix, "");
                var command = this.commandHandler.parseCommand(input);
                if (command != null)
                    command.execute(message, arguments);
            }
        });
    }

    public static void main(String[] args) throws IOException {
        new Raphtalia();
    }
}
