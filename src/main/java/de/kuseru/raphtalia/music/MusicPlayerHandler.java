package de.kuseru.raphtalia.music;

import com.mewna.catnip.entity.channel.TextChannel;
import com.mewna.catnip.entity.guild.Guild;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public class MusicPlayerHandler {

    @Getter
    private Map<Long, MusicPlayer> musicPlayers;

    public MusicPlayerHandler() {
        this.musicPlayers = new HashMap<>();
    }

    public MusicPlayer musicPlayer(Guild guild, TextChannel textChannel) {
        if (hasPlayer(guild))
            return this.musicPlayers.get(guild.idAsLong());
        var musicPlayer = new MusicPlayer(guild, textChannel);
        this.musicPlayers.put(guild.idAsLong(), musicPlayer);
        return musicPlayer;
    }

    private boolean hasPlayer(Guild guild) {
        return this.musicPlayers.containsKey(guild.idAsLong());
    }
}
