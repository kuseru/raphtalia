package de.kuseru.raphtalia.music;

import com.github.natanbc.catnipvoice.AudioProvider;
import com.sedmelluq.discord.lavaplayer.format.StandardAudioDataFormats;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.track.playback.AudioFrame;
import com.sedmelluq.discord.lavaplayer.track.playback.ImmutableAudioFrame;

import javax.annotation.Nonnull;
import java.nio.ByteBuffer;

public class AudioPlayerAudioProvider implements AudioProvider {

    private final ByteBuffer byteBuffer = ByteBuffer.allocate(StandardAudioDataFormats.DISCORD_OPUS.maximumChunkSize());
    private final AudioPlayer audioPlayer;
    private AudioFrame lastFrame;

    public AudioPlayerAudioProvider(AudioPlayer audioPlayer) {
        this.audioPlayer = audioPlayer;
    }

    @Override
    public boolean canProvide() {
        return (lastFrame = audioPlayer.provide()) != null;
    }

    @Nonnull
    @Override
    public ByteBuffer provide() {
        if (lastFrame instanceof ImmutableAudioFrame)
            lastFrame.getData(byteBuffer.array(), lastFrame.getDataLength());
        else
            lastFrame.getData(byteBuffer.array(), 0);
        return byteBuffer.position(0).limit(lastFrame.getDataLength());
    }

    @Override
    public boolean isOpus() {
        return true;
    }
}
