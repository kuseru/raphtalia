package de.kuseru.raphtalia.music;

import com.github.natanbc.catnipvoice.CatnipVoice;
import com.mewna.catnip.entity.builder.EmbedBuilder;
import com.mewna.catnip.entity.channel.TextChannel;
import com.mewna.catnip.entity.channel.VoiceChannel;
import com.mewna.catnip.entity.guild.Guild;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

public class MusicPlayer {

    private Guild guild;
    private TextChannel textChannel;
    private TrackHandler trackHandler;

    MusicPlayer(Guild guild, TextChannel textChannel) {
        this.guild = guild;
        this.textChannel = textChannel;
        this.trackHandler = new TrackHandler(guild.catnip());
        Objects.requireNonNull(this.guild.catnip().extensionManager().extension(CatnipVoice.class))
                .setAudioProvider(guild.id(), new AudioPlayerAudioProvider(this.trackHandler.audioPlayer()));
    }

    public void connect(VoiceChannel voiceChannel) {
        voiceChannel.openVoiceConnection();
    }

    public void loadTrack(String input) {
        System.out.println(input);
        if (!input.startsWith("http://") && !input.startsWith("https://"))
            input = "ytsearch: " + input;
        System.out.println(input);
        this.trackHandler.audioPlayerManager().loadItem(input, new AudioLoadResultHandler() {
            @Override
            public void trackLoaded(AudioTrack track) {
                trackHandler.queueTrack(track);

                String thumbnail;
                try {
                    thumbnail = "https://img.youtube.com/vi/" + getParamValue(track.getInfo().uri, "v") + "/1.jpg";
                } catch (URISyntaxException e) {
                    thumbnail = "";
                }
                var embedBuilder = embedBuilder()
                        .title(track.getInfo().title)
                        .thumbnail(thumbnail)
                        .field("Duration", formatDuration(track.getDuration()), true);
                textChannel.sendMessage(embedBuilder.build());
            }

            @Override
            public void playlistLoaded(AudioPlaylist playlist) {
                if (playlist.isSearchResult()) trackLoaded(playlist.getTracks().get(0));
                playlist.getTracks().forEach(audioTrack -> trackHandler.queueTrack(audioTrack));
            }

            @Override
            public void noMatches() {
                var embedBuilder = embedBuilder()
                        .title("nothing matched with that identifier!");
                textChannel.sendMessage(embedBuilder.build());
            }

            @Override
            public void loadFailed(FriendlyException exception) {
                var embedBuilder = embedBuilder()
                        .title("an error occurred while loading a track!")
                        .description(exception.getMessage());
                textChannel.sendMessage(embedBuilder.build());
            }
        });
    }

    private String formatDuration(long duration) {
        long seconds = duration / 1000;
        long hours = Math.floorDiv(seconds, 3600);
        seconds = seconds - (hours * 3600);
        long mins = Math.floorDiv(seconds, 60);
        seconds = seconds - (mins * 60);
        return (hours == 0 ? "" : hours + ":") + String.format("%02d", mins) + ":" + String.format("%02d", seconds);
    }

    private String getParamValue(String url, String parameter) throws URISyntaxException {
        List<NameValuePair> queryParams = new URIBuilder(url).getQueryParams();
        return queryParams.stream()
                .filter(param -> param.getName().equalsIgnoreCase(parameter))
                .map(NameValuePair::getValue)
                .findFirst()
                .orElse("");
    }

    private EmbedBuilder embedBuilder() {
        return new EmbedBuilder().color(0xa36f4b);
    }

    public TrackHandler trackHandler() {
        return this.trackHandler;
    }
}
