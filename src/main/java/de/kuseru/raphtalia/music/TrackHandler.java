package de.kuseru.raphtalia.music;

import com.mewna.catnip.Catnip;
import com.mewna.catnip.entity.user.Presence;
import com.sedmelluq.discord.lavaplayer.format.StandardAudioDataFormats;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.source.youtube.YoutubeAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class TrackHandler extends AudioEventAdapter {

    private Catnip catnip;
    private AudioPlayerManager audioPlayerManager;
    private final AudioPlayer audioPlayer;
    private Queue<AudioTrack> trackQueue;
    private AudioTrack previousTrack;

    TrackHandler(Catnip catnip) {
        this.catnip = catnip;
        this.audioPlayerManager = new DefaultAudioPlayerManager();
        this.audioPlayerManager.getConfiguration().setOutputFormat(StandardAudioDataFormats.DISCORD_OPUS);
        this.audioPlayerManager.registerSourceManager(new YoutubeAudioSourceManager());
        AudioSourceManagers.registerRemoteSources(audioPlayerManager);
        this.audioPlayer = this.audioPlayerManager.createPlayer();
        this.audioPlayer.addListener(this);
        this.trackQueue = new LinkedBlockingQueue<>();
        this.previousTrack = null;
    }

    void queueTrack(AudioTrack audioTrack) {
        if (this.audioPlayer.getPlayingTrack() != null)
            this.trackQueue.offer(audioTrack);
        else
            this.audioPlayer.playTrack(audioTrack);
    }

    public void forcePlay(AudioTrack audioTrack) {
        if (audioTrack != null)
            this.audioPlayer.playTrack(audioTrack);
    }

    private void nextTrack() {
        AudioTrack audioTrack = this.trackQueue.poll();
        if (audioTrack != null)
            this.audioPlayer.playTrack(audioTrack);
    }

    @Override
    public void onTrackStart(AudioPlayer player, AudioTrack track) {
        this.catnip.presence(Presence.OnlineStatus.ONLINE, track.getInfo().title, Presence.ActivityType.LISTENING, null);
    }

    @Override
    public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
        this.previousTrack = track;

        if (endReason.mayStartNext)
            nextTrack();
    }

    @Override
    public void onTrackException(AudioPlayer player, AudioTrack track, FriendlyException exception) {
        nextTrack();
    }

    public AudioTrack previousTrack() {
        return this.previousTrack;
    }

    AudioPlayerManager audioPlayerManager() {
        return this.audioPlayerManager;
    }

    public AudioPlayer audioPlayer() {
        return this.audioPlayer;
    }
}
