package de.kuseru.raphtalia.util;

import java.text.NumberFormat;

public class RuntimeUtil {

    private static final int MB = 1024 * 1024;
    private static final Runtime runtime = Runtime.getRuntime();
    private static final NumberFormat numberFormat = NumberFormat.getInstance();

    public static double maxMemory() {
        return Double.parseDouble(numberFormat.format(runtime.maxMemory() / MB));
    }

    public static double totalMemory() {
        return Double.parseDouble(numberFormat.format(runtime.totalMemory() / MB));
    }

    public static double freeMemory() {
        return Double.parseDouble(numberFormat.format(runtime.freeMemory() / MB));
    }

    public static double usedMemory() {
        return totalMemory() - freeMemory();
    }
}
